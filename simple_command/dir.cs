using System.IO;

// ref.
// https://msdn.microsoft.com/ko-kr/library/system.environment(v=vs.110).aspx
// https://msdn.microsoft.com/ko-kr/library/system.io.directory_methods(v=vs.110).aspx
// 

var args = Environment.GetCommandLineArgs();

string dirname = null;
if (args.Length <= 3) {
    dirname = ".";
} else {
    dirname = args[3];
}

try {
    var filelist = Directory.GetFileSystemEntries(dirname);
    foreach (var file in filelist) {
        Console.Write("{0}\t", file);
    }
    Console.WriteLine();
} catch (System.Exception) {
    Console.WriteLine("No such file or directory");
}
